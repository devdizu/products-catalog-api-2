const express = require('express');
const router = express.Router();
const Response = require('../classes/response');

//Consultar Categorias
router.get('/', function(req, res) {
	res.status(200).send(
		new Response("The API is working on it.")
	);
});

module.exports = router;